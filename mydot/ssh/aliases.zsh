#!/bin/zsh
alias pubkey="more ~/.ssh/basebox-key.pub | pbcopy | echo '=> Public key copied to pasteboard.'"
