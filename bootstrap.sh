#!/usr/bin/env bash

echo ""
echo "      ___       ___          ___          ___                   ___      "
echo "     /  /\     /__/\        /  /\        /  /\         ___     /  /\     "
echo "    /  /::\    \  \:\      /  /:/_      /  /:/_       /  /\   /  /::\    "
echo "   /  /:/\:\    \  \:\    /  /:/ /\    /  /:/ /\     /  /:/  /  /:/\:\   "
echo "  /  /:/~/:/_____\__\:\  /  /:/_/::\  /  /:/ /::\   /  /:/  /  /:/~/:/   "
echo " /__/:/ /://__/::::::::\/__/:/__\/\:\/__/:/ /:/\:\ /  /::\ /__/:/ /:/___ "
echo " \  \:\/:/ \  \:\~~\~~\/\  \:\ /~~/:/\  \:\/:/~/://__/:/\:\\  \:\/:::::/ "
echo "  \  \::/   \  \:\  ~~~  \  \:\  /:/  \  \::/ /:/ \__\/  \:\\  \::/~~~~  "
echo "   \  \:\    \  \:\       \  \:\/:/    \__\/ /:/       \  \:\\  \:\      "
echo "    \  \:\    \  \:\       \  \::/       /__/:/         \__\/ \  \:\     "
echo "     \__\/     \__\/        \__\/        \__\/                 \__\/     "
echo ""
echo "        ..........................................................       "
echo "        . Dotfiles 0.1.6 (Pongstr) for setting up OSX Workspace  .       "
echo "        .      https://github.com/pongstr/dotfiles.git           .       "
echo "        ..........................................................       "
echo ""

# To run this, you must download & install the latest Xcode and Commandline Tools
# https://developer.apple.com/xcode/
# https://developer.apple.com/downloads/

[[ "$(uname -s)" != "Darwin" ]] && exit 0
  echo ""
  echo "  To run this, you must download & install the latest Xcode and Commandline Tools"
  echo "    > https://developer.apple.com/xcode/"
  echo "    > https://developer.apple.com/downloads/"
  xcode-select --install
    
  mkdir -p $HOME/Workspace/go/src/github.com/user
  WS=$HOME/Workspace
  DOTSH=$HOME/Workspace/.dotsh
  git clone --recursive https://gitlab.com/baseboxorg/dotsh.git $WS/.dotsh
  # Function to check if a package exists
  check () { type -t "${@}" > /dev/null 2>&1; }

  # Function to install Homebrew Formulas:
  install_formula () {
    echo ""
    echo "Installing Homebrew Packages:"
  
    echo ""
    echo "  ➜ tap homebrew/dupes"
    brew tap homebrew/dupes
  
    echo ""
    echo "  ➜ tap homebrew/versions"
    brew tap homebrew/versions
    
    echo ""
    echo "  ➜ tap homebrew/binary"
    brew tap homebrew/binary
    
    echo ""
    echo "  ➜ coreutils, moreutils, findutils"
    brew install coreutils
    sudo ln -s /usr/local/bin/gsha256sum /usr/local/bin/sha256sum
    brew install moreutils
    brew install findutils
    
    echo ""
    echo "  ➜ grc"
    brew install grc
    
    echo ""
    echo "  ➜ readline"
    brew install readline
  
    echo ""
    echo "  ➜ autoconf"
    brew install autoconf
  
    echo ""
    echo "  ➜ pkg-conf"
    brew install pkg-config
    
    echo ""
    echo "  ➜ gnu-sed"
    brew install gnu-sed --with-default-names
    
    echo ""
    echo "  ➜ rbenv"
    brew install rbenv
    brew install ruby-build
    export RBENV_ROOT="$HOME/.RBENV"
    export PATH="$RBENV_ROOT/bin:$PATH"
    eval "$(rbenv init -)"
 
        echo ""
    echo "  ➜ zsh"
    brew install zsh
    
    echo ""
    echo "  ➜ vim (overriding system vim)"
    brew install vim --with-lua --with-luajit --override-system-vi
    brew install macvim --with-lua --with-luajit
    
    echo ""
    echo "  ➜ wget"
    brew install brew install wget --with-iri
    
    echo ""
    echo "  ➜ ack"
    brew install ack
    
    echo ""
    echo "  ➜ Install htop-osx"
    brew install htop-osx

    echo ""
    echo "  ➜ openssl"
    brew install openssl

    echo ""
    echo "  ➜ git"
    brew install git
    brew install git-flow gist hub gh tree grep tree screen hg 
    
    echo ""
    echo "  ➜ imagemagick"
    brew install imagemagick --with-webp
 
    echo ""
    echo "  ➜ pyenv"
    brew install pyenv pyenv-virtualenv pyenv-virtualenvwrapper pyenv-pip-rehash
    export PYENV_ROOT="$HOME/.pyenv"
    export PATH="$PYENV_ROOT/bin:$PATH"
    eval "$(pyenv init -)"
    eval "$(pyenv virtualenv-init -)"
    
    echo ""
    echo "  ➜ node"
    brew install node --without-npm
    echo prefix=${HOME}/local/npm >> ~/.npmrc
    mkdir -p ${HOME}/local/npm
    curl -L https://www.npmjs.org/install.sh | sh
    export NPM_HOME="$HOME/local/npm"
    export PATH="$NPM_HOME/bin:$PATH"
    
    echo ""
    echo "  ➜ libyaml"
    brew install libyaml
    
    echo ""
    echo "  ➜ xz"
    brew install xz
    
    echo ""
    echo "  ➜ gcc"
    brew install gcc
    
    echo ""
    echo "  ➜ Golang"
    brew install go
    
    echo ""
    echo "  ➜ Docker"
    brew install docker
    
    echo ""
    echo "  ➜ Packer"
    brew install packer

    # Cleanup
    echo ""
    echo "Cleaning up Homebrew installation..."
    brew cleanup

    cp -R $DOTSH/bin/shell/.zshrc $HOME/.zshrc
    source ~/.zshrc
    
    echo "Installing Caskroom, Caskroom versions and Caskroom Fonts..."
    brew tap caskroom/homebrew-cask
    brew install brew-cask
    
    brew tap caskroom/versions
    brew tap caskroom/fonts

    # Make /Applications the default location of apps
    export HOMEBREW_CASK_OPTS="--appdir=/Applications --caskroom=/opt/homebrew-cask/caskroom"

    echo ""
    echo "Installing monospace fonts... "
    brew cask install font-droid-sans-mono
    brew cask install font-ubuntu
    brew cleanup
  } 


# Install Hushlogin
echo ""
echo "Install hushlogin"
echo "  - Disable the system copyright notice, the date and time of the last login."
echo "    more info at @mathiasbynens/dotfiles http://goo.gl/wZBM80"
echo ""
cp -f "$DOTSH/.hushlogin" $HOME/.hushlogin


# Install Homebrew
# ---------------------------------------------------------------------------
echo ""
echo "Checking if Homebrew is installed..."

if check brew; then
  echo "Awesome! Homebrew is installed! Now updating..."
  echo ""
  brew upgrade
  brew update
fi

if ! check brew; then
  echo "Download and install homebrew"
  echo ""
  ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"

  # Run Brew doctor before anything else
  brew doctor
fi

# Install Homebrew Formulas
while true; do
  read -p "Would you like to install Homebrew formulas? [y/n] " answer
  echo ""
  case $answer in
    [y]* ) install_formula; break;;
    [n]* ) break;;
    * ) echo "Please answer Y or N.";;
  esac
done