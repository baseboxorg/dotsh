#!/bin/zsh

alias gob='go build'
alias goc='go clean'
alias gog='go get'
alias goi='go install'
alias gol='go list'
alias gor='go run'
alias got='go test'
alias go.='cd $GOPATH'
alias gosrc.='cd $GOPATH/src'
alias gogh.='cd $GOPATH/src/github.com'

#go get golang.org/x/tools/cmd/vet
#go get golang.org/x/tools/cmd/godoc
#go get github.com/tools/godep
#go get github.com/mitchellh/gox
#go get -u github.com/gpmgo/gopm

