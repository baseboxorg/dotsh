#!/bin/zsh
export PATH="$WS/bin:usr/local/bin:/usr/local/sbin:/usr/bin:/usr/sbin:/bin:$(brew --prefix coreutils)/libexec/gnubin:$GOPATH/bin:$ZSH/bin:$PATH"
export MANPATH="/usr/local/man:/usr/local/mysql/man:/usr/local/git/man:$MANPATH"


export PYENV_ROOT="$HOME/.pyenv"
export PATH="$PYENV_ROOT/bin:$PATH"
export PATH="$HOME/.rbenv/shims:$PATH"
