#!/bin/zsh


###############
# Sublime & Atom
###############
alias st="subl"
alias s.="subl ."
alias at="atom"
alias a.="atom ."

################
# boot2docker
################

################
# docker 
################
alias dk='docker'
alias cleandk='docker rm $(docker ps -a -q); docker rmi $(docker images | grep "^<none>" | awk "{print $3}")'
alias dka='docker ps -a'
alias dkm='docker images'
alias dkrm='docker rm '
alias dkrm!='docker rm -f '
alias dkrmi='docker rmi '
alias dkrmi!='docker rmi -f '
alias dkb='docker build '
alias dkt='docker tap '
alias dkps='docker push '
alias dkpl='docker pull '
alias killdk='docker stop $(docker ps -a -q); docker rm $(docker ps -a -q)'
alias dkip='docker inspect -f "{{ .NetworkSettings.IPAddress }}" '
alias dkck='docker inspect -f "{{.State.Pid}}" '
# Kill all running containers.
alias killcdk='docker kill $(docker ps -a -q)'
# Delete all stopped containers.
alias cleancdk='docker rm $(docker ps -a -q)'
# Delete all untagged images.
alias cleanidk='docker rmi $(docker images -q -f dangling=true)'
# Delete all stopped containers and untagged images.
alias cleanalldk='dockercleanc || true && dockercleani'


################
# vagrant 
################
alias vv='vagrant'
alias vup='vagrant up'
alias vuaw='vagrant up --provider=aws'
alias vuvb='vagrant up --provider=virtualbox'
alias vuvf='vagrant up --provider=vmware_fusion'
alias vudk='vagrant up --provider=docker'
alias vudo='vagrant up --provider=digital_ocean'
alias vpr='vagrant provision'
alias vd!='vagrant destroy --force'
alias vsp='vagrant suspend'
alias vrs='vagrant resume'
alias vrl='vagrant reload'
alias vrp='vagrant reload --provision'
alias vst='vagrant status'
alias vssh='vagrant ssh'
alias vsc='vagrant ssh-config'
alias vpl='vagrant plugin list'
alias vpud='vagrant plugin list update'
alias vpi='vagrant plugin install'
alias vpu='vagrant plugin uninstall'
alias vbxl='vagrant box list'
alias vbxa='vagrant box add'
alias vbxr='vagrant box remove'
alias vbxo='vagrant box outdated'
alias vbxu='vagrant box update'
alias vgs='vagrant global-status'
alias vgsp='vagrant global-status --prune'
alias vdoi='vagrant digitalocean-list images $DIGITAL_OCEAN_TOKEN'
alias vdor='vagrant digitalocean-list regions $DIGITAL_OCEAN_TOKEN'
alias vdos='vagrant digitalocean-list sizes $DIGITAL_OCEAN_TOKEN'


 
################
# coreos 
################

################
# fig
################

################
# npm
################

################
# pyenv
################

################
# heroku
################

################
# aws
################

################
# google-cloud
################

################
# rhc
################

################
# rhc
################

################
# ansible
################

################
# puppet
################

################
# chef
################

################
# vmrun
################

################
# sql
################
 
################
# test kitchen
################
alias k="kitchen"
alias kc="kitchen converge"
alias kd="kitchen destroy"
alias ks="kitchen list"
alias kl="kitchen login"
alias kt="kitchen test"
 
#############
# VirtualBox
#############
alias vlr="VBoxManage  list runningvms"
alias vlv="VBoxManage  list vms"


##########
# PS
#########
alias psa="ps aux"
alias psg="ps aux | grep "
alias psr='ps aux | grep ruby'

# Moving around
alias cdb='cd -'
alias cls='clear;ls'

# Show human friendly numbers and colors
alias df='df -h'
alias du='du -h -d 2'

if [[ $platform == 'linux' ]]; then
  alias ll='ls -alh --color=auto'
  alias ls='ls --color=auto'
elif [[ $platform == 'darwin' ]]; then
  alias ll='ls -alGh'
  alias ls='ls -Gh'
fi

# show me files matching "ls grep"
alias lsg='ll | grep'



##########
# MVIM
#########
mvim --version > /dev/null 2>&1
MACVIM_INSTALLED=$?
if [ $MACVIM_INSTALLED -eq 0 ]; then
  alias vim="mvim -v"
fi

# mimic vim functions
alias :q='exit'
# vimrc editing
alias ve='vim ~/.vimrc'
alias ze='vim ~/.zshrc'
alias zr='source ~/.zshrc'

##########
# Git
#########

##################
# SHELL Functions
##################
alias less='less -r'
alias tf='tail -f'
alias l='less'
alias lp='lesspipe'
alias lh='ls -alt | head' # see the last modified files
alias screen='TERM=screen screen'
alias cl='clear'

##########
# Ruby
#########


alias ka9='killall -9'
alias k9='kill -9'

##########
# Gem
#########
alias sgi='sudo gem install --no-ri --no-rdoc'



##########
# Finder
#########
alias showFiles='defaults write com.apple.finder AppleShowAllFiles YES; killall Finder /System/Library/CoreServices/Finder.app'
alias hideFiles='defaults write com.apple.finder AppleShowAllFiles NO; killall Finder /System/Library/CoreServices/Finder.app'


##########
# Update
#########
alias reload!="source ~/.zshrc"
alias update!="sudo softwareupdate -i -a && gem update --system && npm install -g npm@latest && brew update && brew upgrade && brew-cask update && brew cleanup && brew cask cleanup &&  brew prune && brew doctor"
#alias odb='open ~/Applications/dropbox'


##########
# CD Path
#########
alias lapp.="cd $HOME/Applications"
alias gapp.="cd /Applications"
alias dt.="cd $HOME/Desktop"
alias doc.="cd $HOME/Documents"
alias dl.="cd $HOME/Downloads"
alias db.="cd $HOME/Dropbox"
alias util.="cd /Applications/Utilities"
alias ssh.="cd $HOME/.ssh"
alias w.="cd $WS"
alias dot.="cd $WS/.dotfiles"
alias zsh.="cd $ZSH"
alias gbin.="cd /usr/local/bin"
alias lbin.="cd $WS/bin"


alias demo.="cd $WS/Demo"
alias site.="cd $WS/Sites"
alias cc.="cd $WS/credentials"
alias p.="cd $WS/Projects"
alias b.="cd $WS/Build"
alias v.="cd $WS/Vagrant"
alias d.="cd $WS/Docker"
alias df.="cd $WS/Docker/dockerfiles"
alias dfm.="cd $WS/Docker/dockerfiles/managed"
alias pp.="cd $WS/Packer"
alias osx.="cd $WS/OSX"
alias go.="cd $WS/Go"
alias data.="cd $WS/Data"
alias code.="cd $WS/Codes"
alias gh.="cd $WS/Codes/github"
alias gl.="cd $WS/Codes/gitlab"
alias bb.="cd $WS/Codes/bitbucket"
alias lc.="cd $WS/Codes/local"


#############
# Open Apps
#############
alias odb='open ~/Applications/Dropbox.app'
alias ogd='open ~/Applications/"Google Drive.app"'
alias oas='open /Applications/"App Store.app"'

##########
# SSH
#########
alias ssh-ob="pbcopy < ~/.ssh/openbase-key.pub"
alias ssh-bb="pbcopy < ~/.ssh/basebox-key.pub"
alias ssh-id="pbcopy < ~/.ssh/id_rsa.pub"

alias srm="sudo rm -R"
alias rmh="rm $HOME/.ssh/known_hosts"
alias rmf="sudo rm -R $HOME/.fleetctl"

alias ezsh="subl ~/.zshrc"
alias econf="subl $HOME/.ssh/config"

##########
# Apps
#########
alias pp="packer"
alias vv="vagrant"
alias dd="docker"
alias b2="boot2docker"
