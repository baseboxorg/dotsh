# Export PATH after HomeBrew, RBENV, PYENV installation
export PATH="$RBENV_ROOT/bin:$PYENV_ROOT/bin:$NPM_HOME/bin:$HOME/bin:/usr/local/bin:/usr/local/sbin:/usr/bin:/usr/sbin:/usr/local/opt/gnu-tar/libexec/gnubin:$DOTSH/bin:$PATH"

# Make /Applications the default location of apps
export HOMEBREW_CASK_OPTS="--appdir=/Applications --caskroom=/opt/homebrew-cask/caskroom"

# Export RBENV root's path and env
export RBENV_ROOT="$HOME/.RBENV"
if which rbenv > /dev/null; then eval "$(rbenv init -)"; fi

# Export Pyenv root's path and env
export PYENV_ROOT="$HOME/.pyenv"
if which pyenv > /dev/null; then eval "$(pyenv init -)"; fi
if which pyenv-virtualenv-init > /dev/null; then eval "$(pyenv virtualenv-init -)"; fi

# Export npm path
export NPM_HOME="$HOME/local/npm"


