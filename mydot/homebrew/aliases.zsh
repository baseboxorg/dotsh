#!/bin/zsh
brewbump() {
  brew update
  brew upgrade
  brew cask update
  brew cleanup
  brew cask cleanup
  brew prune
  brew doctor
}
